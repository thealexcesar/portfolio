This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Portfolio - Next.js

## This repository contains the source code for my portfolio. It includes information about me, my projects, skills, and experiences.

<a href="https://alexcesar.dev" target="_blank">View Live Demo</a>

[//]: # (TODO ADD IMAGE)

## Getting Started

Ensure that you have Node.js installed on your system.

1. Clone this repository:

```bash
git clone https://github.com/thealexcesar/portfolio.git
```


## Libraries

- **[next](https://nextjs.org/docs/getting-started):** React framework for production.
- **[react](https://reactjs.org/docs/getting-started.html):** Library for building user interfaces in React.
- **[react-dom](https://reactjs.org/docs/react-dom.html):** Responsible for rendering the DOM for the web.
- **[react-icons](https://react-icons.github.io/react-icons/):** Package of icons for React.
- **[sass](https://sass-lang.com/documentation):** CSS extension offering powerful features.
- **[framer-motion](https://www.framer.com/api/motion/):** Library for fluid and expressive animations in React.



You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
